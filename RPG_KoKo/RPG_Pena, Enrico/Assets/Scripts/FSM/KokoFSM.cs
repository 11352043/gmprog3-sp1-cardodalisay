﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class KokoFSM : FSM {

	public enum FSMState
	{
			Idle,
			Run,
			Chase,
			Attack,
			Hurt,
			Dead
	}

	public FSMState curState;
	public KokoAnim kokoAnim;
	NavMeshAgent agent;
	public GameObject marker;
	GameObject markerPrefab;
	RaycastHit hit;

    public Text healthText;
	public Text CurrLvl;
	public Text Attack;
	public Text GoldText;
	public Text ExpText;
    public float MaxExp;
	public float CurrExp;
	public float GoldAmount;
	public float Stat;

	public float totalHealth;
	public float totalDamage;

	

	SpawnScript SpawnScript;

	public GameObject target;

	public List<GameObject> EnemiesInRange = new List<GameObject>();

	protected override void Initialize ()
	{
		  SpawnScript = GameObject.FindObjectOfType<SpawnScript>();
		  curState = FSMState.Idle;
		  kokoAnim = GetComponent<KokoAnim>();
		  agent = GetComponent<NavMeshAgent>();
		  Strength = 2;
		  Damage = 2;
		  Vitality = 2;
		  MaxHealth = 100;
		  Health = MaxHealth;
		  Stat++;
		 
	}

	protected override void FSMUpdate ()
	{
			switch (curState) {
			case FSMState.Idle:
					UpdateIdleState ();
					break;
			case FSMState.Run:
					UpdateRunState ();
					break;
			case FSMState.Attack:
					UpdateAttackState ();
					break;
			case FSMState.Hurt:
					UpdateHurtState ();
					break;
            /*case FSMState.Died:
                UpdateHurtState();
                break;*/

        }
	}

	

	public void HurtState()
	{
		curState = FSMState.Hurt;
	}

	public void NextLevel()
	{
		
		if (CurrExp >= MaxExp)
		{
			Level += 1;
			
			MaxExp += MaxExp / 2;
			CurrExp = 0;
			
			MaxHealth += Level *  Vitality +   MaxHealth;
			Damage += Damage + Strength + Level * 2;
		}
	}

	
	protected override void FSMFixedUpdate()
	{
		AgentHasArrived();
		
		totalDamage = Strength * 2 + Damage;
		
		healthText.text = "Health = " + Health.ToString() +  "/" + MaxHealth.ToString();
		
		CurrLvl.text = "Level = " + Level;
		GoldText.text = "Gold = " + GoldAmount;
		
		ExpText.text = "EXP = " + CurrExp;
		
		NextLevel();
		 
		if (Input.GetMouseButtonDown(0)) 
		{
			RemoveMarker();
			curState = FSMState.Run;
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
			{
				
				
					if (hit.collider.tag == "Enemy")
					{
					   target = hit.collider.gameObject;
					   agent.destination = hit.point;
					   curState = FSMState.Attack;
					   return;
					}

					agent.destination = hit.point;

					InstantiateMarker();
			}
		}
	}

	protected void UpdateIdleState()
	{
		 kokoAnim.AnimState(0,0);
	}

	protected void UpdateRunState()
	{
		 kokoAnim.AnimState(1,0);
		 float distance = Vector3.Distance (transform.position, agent.destination);

		 if (distance <= 1)
		 {
			curState = FSMState.Attack;
		 }	 
	}

	void DamageTo()
	{
		if(target != null)
		{
			target.GetComponent<MonsterFSM>().Health -= Damage; 
			target.GetComponent<MonsterFSM>().HurtState();
		}
	}

	protected void UpdateAttackState()
	{
	   kokoAnim.AnimState(1,0);

	   if (target != null)
	   {
			float distance = Vector3.Distance (transform.position, target.transform.position);
			
			if (distance <= 2.0f)
			{
				kokoAnim.AnimState(2,0);
			}
	   }

	}
		public void Died()
	{
        //if (Health <= 0)
        Destroy(this.gameObject);
        Debug.Log("Died");
	}

    protected void UpdateHurtState()
    {
        if (Health <= 0)
        {
            curState = FSMState.Hurt;
        }

        //else if (Health >= 0)
        //{
        //	kokoAnim.AnimState(5, 0);
        //	EnemyInRadius(transform.position,3f);
        //}
    }



    void EnemyInRadius(Vector3 center, float radius)
    {
	  Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
	  foreach (Collider col in hitColliders)
	  {
		if (col.tag == "Enemy")
		{
			target = col.gameObject;
			agent.destination = col.transform.position;
			curState = FSMState.Run;
		}
	  }
    }

	void AgentHasArrived()
    {
        if (!agent.pathPending)
        {
            //if remaining distance of path is less than or equal to stopping distance of agent
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                //if agent does not have path and the velocity is 0 or it already stopped 
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                {
					curState = FSMState.Idle;
                    //destroy the marker prefab after 
                    RemoveMarker();
                }
            }
        }
    }

	void InstantiateMarker()
    {
        //instantiate the marker prefab game object  marker = game object prefab, hit.point = where you click the mouse, transform rotation = transform rotation of this game object
        markerPrefab = (GameObject) Instantiate(marker, hit.point, transform.rotation);
    }

	void RemoveMarker()
    {
        Destroy(markerPrefab);
    }

}
