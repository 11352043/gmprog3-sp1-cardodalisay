﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour {
    NavMeshAgent agent;
    public GameObject marker;
    GameObject markerPrefab;
    KokoAnim kokoAnim;
    RaycastHit hit;
    
	void Start () {

        agent = GetComponent<NavMeshAgent>();
        kokoAnim = GetComponent<KokoAnim>();
	}

    // Update is called once per frame
    void Update() {

        AgentHasArrived();

        if (Input.GetMouseButtonDown(0)) {
          
            //remove the previous marker before instantiating marker
            RemoveMarker();

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
            {
                //agent destination is equal to where you click the mouse pointer which is the hit point
                agent.destination = hit.point;
                //instantiates marker to where the hit point is
                InstantiateMarker();
            }
        }
    }

    void AgentHasArrived()
    {
        if (!agent.pathPending)
        {
            State(1,1);
            //if remaining distance of path is less than or equal to stopping distance of agent
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                //if agent does not have path and the velocity is 0 or it already stopped 
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                {
                     State(0,0);
                    //destroy the marker prefab after 
                    RemoveMarker();
                }
            }
        }
    }

    void InstantiateMarker()
    {
        //instantiate the marker prefab game object  marker = game object prefab, hit.point = where you click the mouse, transform rotation = transform rotation of this game object
        markerPrefab = (GameObject) Instantiate(marker, hit.point, transform.rotation);
    }

    void State(int anim, float blend)
    {
          kokoAnim.AnimationTransition(anim);
          kokoAnim.BlendTransition(blend);
    }

    void RemoveMarker()
    {
        Destroy(markerPrefab);
    }
  }

