﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;
    public float delay;
    public Vector3 offset;

    void Start()
    {
        transform.position = player.position + offset;
    }

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, player.position + offset, delay * Time.deltaTime);
    }
}