﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour
{

    public GameObject[] prefab;
    public GameObject[] gos;

    public GameObject[] spawnPoints;
    float timer = 10;

    void Start()
    {
        spawnPoints = GameObject.FindGameObjectsWithTag("WandarPoint");

        gos = new GameObject[30];

        for (int i = 0; i < gos.Length; i++)
        {
            GameObject clone = (GameObject)Instantiate(prefab[Random.Range(0, prefab.Length)], spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position, Quaternion.identity);
            gos[i] = clone;
        }
    }

    void Update()
    {

    }
}
