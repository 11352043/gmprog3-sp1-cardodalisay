﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Skills : MonoBehaviour {

	public GameObject[] SkillAnim; 	

	public float coolDown;


	public int activeSkill;
	KokoFSM kokoFSM;

	public Canvas canvas;

	bool showSkillCanvas;

	void Start()
	{
		kokoFSM = GameObject.FindObjectOfType<KokoFSM>();

		canvas.enabled = showSkillCanvas;
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.K))
		{
			showSkillCanvas = !showSkillCanvas;
			canvas.enabled = showSkillCanvas;
		}
	}

	public void Skill()
	{
		StartCoroutine(StartSkill());
	}

	public void SkillAnimation()
	{
		GameObject skill = (GameObject) Instantiate(SkillAnim[activeSkill],transform.position,transform.rotation);
		skill.transform.SetParent(this.gameObject.transform);
		Destroy(skill, .5f);
	}

	public void ChangeSkillSet(int id)
	{
		activeSkill = id;	
	}

	void Heal()
	{
		if (activeSkill == 1)
		{
	 		kokoFSM.Health += kokoFSM.SkillDamage;
		}

		else
		{
			return;
		}
	
	}

	void AreaDamageEnemies(Vector3 location, float radius, float damage)
	{
		Collider[] objectsInRange = Physics.OverlapSphere(location, radius);
		foreach (Collider col in objectsInRange)
		{
			MonsterFSM enemy = col.GetComponent<MonsterFSM>();
			if (enemy != null)
			{
				// linear falloff of effect
				float proximity = (location - enemy.transform.position).magnitude;
				float effect = 1 - (proximity / radius);
				
				enemy.Health -= damage * effect + kokoFSM.SkillDamage;
				enemy.HurtState();
			}
		}
	}

	void GroundCrush()
	{
		if (activeSkill == 0)	
		{
			AreaDamageEnemies(transform.position, 5f, kokoFSM.SkillDamage);
		}

		else
		{
			return;
		}
	}



	IEnumerator StartSkill()
	{

		if (coolDown >= 60)
		{
			coolDown = 0;
			float time = 0;
			float duration = 60;
			

			SkillAnimation();
			StartCoroutine(Blessing());
			GroundCrush();
			//heal
			Heal();


			while (time < duration)
			{
				coolDown += Time.deltaTime;
				time += Time.deltaTime;
				yield return null;
			}
		}
	}

	IEnumerator Blessing()
	{
		if (activeSkill == 2)
		{
			coolDown = 0;
			float time = 0;
			float duration = 60;

			float lastStrength = kokoFSM.Strength;
			float lastMana = kokoFSM.Mana;
			float lastDamage = kokoFSM.Damage;

			kokoFSM.Strength += kokoFSM.SkillDamage;
			kokoFSM.Damage += kokoFSM.Strength;
			kokoFSM.MaxMana += kokoFSM.SkillDamage;
			kokoFSM.Mana = kokoFSM.MaxMana;
			
		

			while (time < duration)
			{
				coolDown += Time.deltaTime;
				time += Time.deltaTime;
				yield return null;
			}

			kokoFSM.Strength = lastStrength;
			kokoFSM.Mana = lastMana;
			kokoFSM.Damage = lastDamage;
		}

		
	}
	
}
