﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

 public enum FSMState
{
        None,
        Patrol,
        Chase,
        Attack,
        Hurt,
        Dead
}

public class MonsterFSM : FSM
{

      // bali eto yung mga states ng monster tuwing iccal yung enum state yun lang yung gaganang action 
      // kapag cinall yung state na chase yung chase lang gagana depende na lang kung ma grant yung condition na nasa loob ng function na yun
      //para lumipat ng state
       
        //Current state that the Box is in

        public MonsterAnim monsterAnim;
        public NavMeshAgent agent;
        public FSMState curState;
        public GameObject target;
        private float timer;
        public Image healthImage;
        public GameObject[] points;
        public int radius;
        public Collider[] hitColliders;
        public int destPoint = 0;
        public float MinExpGiven,MaxExpGiven;
        public float MinGoldGiven,MaxGoldGiven;




        //ETONG INITIALIZE PARANG START THEN PERO ANG PINAGKAIBA NILA ETONG INITIALIZE CINALL DUN SA BASE CLASS NA FSM KASE PAG NAGLAGAY NG START FUNCTION
        //DITO SA CLASS NA TO MAGKAKAPROBLEMA
        protected override void Initialize ()
        {
                //eto yung unang state ng monster since cinall siya sa intialize functino
                //eto yung start ng fsm
                //
                curState = FSMState.Patrol;

                //agent kinuha yung component ng navmesh kase gagamitin to sa chase function
                 agent = GetComponent<NavMeshAgent>();

                 Health = 20;
                 MaxHealth = 20;
                 Damage = 2 + Strength;

                 healthImage.fillAmount = Health / MaxHealth;

            // Disabling auto-braking allows for continuous movement
            // between points (ie, the agent doesn't slow down as it
            // approaches a destination point).

                agent.autoBraking = false;
                                 
                //Get the list of points
                points = GameObject.FindGameObjectsWithTag ("WandarPoint");
                
                //Set Random destination point for the patrol state first
                
                //Get the target enemy(Player)
                GameObject objPlayer = GameObject.FindGameObjectWithTag ("Player");
                //playerTransform = objPlayer.transform;
               // target = playerTransform.gameObject;

                destPoint = Random.Range(0, points.Length);
                
                //if (!playerTransform)
              //  print ("Player doesn't exist.. Please add one " + "with Tag named 'Player'");
                
        }
        //Update each frame

        //ETONG FSM UPDATE DITO GAGANA LAHAT NG STATE TUWING MAGSSWITCH NG FUNCTION 
        //KUNYARE KAPAG Curstate = fsmstate.chase yung chase updatechase state gagana kase dahil nasa loob siya ng fsmstate.chase case
        protected override void FSMUpdate ()
        {
                switch (curState) {
                case FSMState.Patrol:
                        UpdatePatrolState ();
                        break;
                case FSMState.Chase:
                        UpdateChaseState ();
                        break;
                case FSMState.Attack:
                        UpdateAttackState();
                        break;
                 case FSMState.Hurt:
                        UpdateHurtState();
                        break;
                case FSMState.Dead:
                        UpdateDeadState();
                        break;
                }
        }

        public void HurtState()
        {
                curState = FSMState.Hurt;
        }

        //patrol state 
        protected void UpdatePatrolState ()
        {
                monsterAnim.AnimState(1, 0);
                agent.destination = points[destPoint].transform.position;
                EnemyInRadius(transform.position, 4.5F);
                
                 if (!agent.pathPending && agent.remainingDistance < 0.5f)
                 {
                        EnemyInRadius(transform.position, 2.5F);
                        GotoNextPoint();
                 }
        }

          void GotoNextPoint() {
            // Returns if no points have been set up
            if (points.Length == 0)
                return;

            // Set the agent to go to the currently selected destination.
            // Choose the next point in the array as the destination,
            // cycling to the start if necessary.
           destPoint = Random.Range(0,points.Length);
           agent.destination = points[destPoint].transform.position;
        }

        void Died()
        {  
           float expGiven = Random.Range(MinExpGiven,MaxExpGiven);
           //float goldGiven = Random.Range(MinGoldGiven,MaxGoldGiven);
           target.GetComponent<KokoFSM>().CurrExp += expGiven;
           //target.GetComponent<KokoFSM>().GoldAmount += goldGiven;
        
          Rewards rewards = GameObject.FindObjectOfType<Rewards>();
         
          Instantiate(rewards.Coin,transform.position,transform.rotation);
			rewards.DropLoot ();
      //Instantiate (rewards.Drops[range],transform.position,transform.rotation);
		if (rewards.ItemDropped != null) {
			Instantiate (rewards.ItemDropped, transform.position, Quaternion.identity);
		}
          Destroy(this.gameObject);
        }

        protected void UpdateDeadState ()
        {
               monsterAnim.AnimState(7,0);
        }

     
       
       //ETO YUNG CHASE STATE 
        protected void UpdateChaseState ()
        {       
                monsterAnim.AnimState(1,0);
                transform.LookAt(target.transform.position);
                agent.destination = target.transform.position;
                
                float distance = Vector3.Distance (transform.position, agent.destination);

                if (distance <= 2.0)
                {
                        curState = FSMState.Attack;
                }	

                else if (distance > 2.5f)
		{
			curState = FSMState.Patrol;
		} 
        }

        //eto yung attack state
        protected void UpdateAttackState()
        {   
            if (target != null)
            {
                float distance = Vector3.Distance (transform.position, target.transform.position);

                if (distance <= 2.0f)
                {
                        monsterAnim.AnimState(2,0);
                }

                else if (distance >= 1.5)
                {
                   curState = FSMState.Patrol; 
                }
            }
        }

        void DamageTo()
	{
		if(target != null)
		{
			target.GetComponent<KokoFSM>().Health -= Damage; 
			target.GetComponent<KokoFSM>().HurtState();
		}
	}

        protected void UpdateHurtState()
        {  
           if(Health <= 0)
           {
                curState = FSMState.Dead;
           }

           else 
           {
                monsterAnim.AnimState(5, 0);
                EnemyInRadius(transform.position,2f);
                healthImage.fillAmount = Health / MaxHealth;
           }
     
        }

       void EnemyInRadius(Vector3 center, float radius)
        {
                Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
                foreach (Collider col in hitColliders)
                {
                        if (col.tag == "Player")
                        {
                                target = col.gameObject;
                                agent.destination = col.transform.position;
                                curState = FSMState.Chase;
                        }
                }
        }
}