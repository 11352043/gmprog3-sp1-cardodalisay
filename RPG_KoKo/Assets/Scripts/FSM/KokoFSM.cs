﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class KokoFSM : FSM {

	public enum FSMState
	{
			Idle,
			Run,
			Chase,
			Attack,
			Hurt,
			Dead
	}

	public FSMState curState;
	public KokoAnim kokoAnim;
	NavMeshAgent agent;
	public GameObject marker;
	GameObject markerPrefab;
	RaycastHit hit;
	public Text healthText;
	public Text manaText;
	public Text CurrLvl;
	public Text Attack;
	public Text SkillAttack;
	public Text GoldText;
	public Text VitalityText;
	public Text StrengthText;
	public Text ExpText;
	public Text StatText;
	public float MaxExp;
	public float CurrExp;
	public float MaxMana = 100;
	public float Mana = 100;
	public float GoldAmount;
	public float Stat;

	public float totalHealth;
	public float totalDamage;

	public Image ExpBar;

	SpawnScript SpawnScript;

	public GameObject target;

	public List<GameObject> EnemiesInRange = new List<GameObject>();

	protected override void Initialize ()
	{
		  SpawnScript = GameObject.FindObjectOfType<SpawnScript>();
		  curState = FSMState.Idle;
		  kokoAnim = GetComponent<KokoAnim>();
		  agent = GetComponent<NavMeshAgent>();
		  Strength = 2;
		  Damage = 2;
		  Vitality = 1;
		  MaxHealth = 100;
		  Health = MaxHealth;
		  Stat++;
	}

	protected override void FSMUpdate ()
	{
			switch (curState) {
			case FSMState.Idle:
					UpdateIdleState ();
					break;
			case FSMState.Run:
					UpdateRunState ();
					break;
			case FSMState.Attack:
					UpdateAttackState ();
					break;
			case FSMState.Hurt:
					UpdateHurtState ();
					break;
			case FSMState.Dead:
                        UpdateDeadState();
                        break;
			}
	}

	

	public void HurtState()
	{
		curState = FSMState.Hurt;
	}

	public void NextLevel()
	{
		// IF current exp is greather than maxexp
		if (CurrExp >= MaxExp)
		{
			Level += 1;
			//calculation for next level exp
			MaxExp += MaxExp / 2;
			CurrExp = 0;
			//calculation for next level health
			MaxHealth += Level *  Vitality +   MaxHealth;
			MaxMana += Level *  Vitality +  MaxHealth;
			
			Damage += Damage + Strength + Level * 2;
		}
	}

	public void AddStrength()
	{
		if (Stat > 0)
		{
			Strength++;
			Stat--;
			Damage += Strength;
			
		}
	}

	public void AddVitality()
	{
		if (Stat > 0)
		{
			Vitality++;
			Stat--;
			MaxHealth += Vitality;
		}
	}

	protected override void FSMFixedUpdate()
	{
		totalDamage = Strength * 2 + Damage;
		ExpBar.fillAmount = CurrExp / MaxExp;
		healthText.text = "Health = " + Health.ToString() +  "/" + MaxHealth.ToString();
		manaText.text = "Mana = " + Mana.ToString() +  "/" + MaxMana.ToString();
		Attack.text = "Attack = " + totalDamage;
		CurrLvl.text = "Level = " + Level;
		GoldText.text = "Gold = " + GoldAmount;
		SkillAttack.text = "Skil Attack = " + SkillDamage;
		StrengthText.text = "Strength = " + Strength;
		VitalityText.text = "Vitality = " + Vitality;
		StatText.text = "Statpoints = " + Stat;
		ExpText.text = "EXP = " + CurrExp;

		if (Health > MaxHealth)
		{
			Health = MaxHealth;
		}

		//-----------------------------------------------------------------------------------------
		NextLevel();
		 
		if (Input.GetMouseButtonDown(1)) 
		{
			RemoveMarker();
			curState = FSMState.Run;
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
			{
				//curState = FSMState.Chase;
				//agent destination is equal to where you click the mouse pointer which is the hit point
				

					if (hit.collider.tag == "Enemy")
					{
					   target = hit.collider.gameObject;
					   agent.destination = hit.point;
					   curState = FSMState.Attack;
					   return;
					}

					agent.destination = hit.point;

					InstantiateMarker();
			}
		}
	}

	protected void UpdateDeadState ()
	{
		kokoAnim.AnimState(7,0);
		SpawnScript.RespawnPlayer();
	}

	protected void UpdateIdleState()
	{
		 kokoAnim.AnimState(0,0);
		// EnemyInRadius(transform.position, 5f);
	}

	protected void UpdateRunState()
	{
		 kokoAnim.AnimState(1,0);
		 float distance = Vector3.Distance (transform.position, agent.destination);

		 if (distance <= 1)
		 {
			curState = FSMState.Attack;
		 }	 
	}

	void DamageTo()
	{
		if(target != null)
		{
			target.GetComponent<MonsterFSM>().Health -= Damage; 
			target.GetComponent<MonsterFSM>().HurtState();
		}
	}

	protected void UpdateAttackState()
	{
	   kokoAnim.AnimState(1,0);

	   if (target != null)
	   {
			float distance = Vector3.Distance (transform.position, target.transform.position);
			
			if (distance <= 1.0f)
			{
				kokoAnim.AnimState(2,0);
			}

			else if  (distance >= 1.0f)
			{
				target = null;
				curState = FSMState.Idle;
			}
	   }

	   else
	   {
		   curState = FSMState.Idle;
		   RemoveMarker();
	   }

	}
	void Died()
	{
		Destroy(this.gameObject);       
	}

	protected void UpdateHurtState()
	{  
		if(Health <= 0)
		{
			curState = FSMState.Dead;
		}

		else if (Health >= 0)
		{
			kokoAnim.AnimState(5, 0);
			EnemyInRadius(transform.position,3f);
		}
	}


	
	void EnemyInRadius(Vector3 center, float radius)
    {
	  Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
	  foreach (Collider col in hitColliders)
	  {
		if (col.tag == "Enemy")
		{
			target = col.gameObject;
			agent.destination = col.transform.position;
			curState = FSMState.Run;
		}
	  }
    }

	void AgentHasArrived()
    {
			if (!agent.pathPending)
			{
				//if remaining distance of path is less than or equal to stopping distance of agent
				if (agent.remainingDistance <= agent.stoppingDistance)
				{
					//if agent does not have path and the velocity is 0 or it already stopped 
					if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
					{
						curState = FSMState.Idle;
						//destroy the marker prefab after 
						RemoveMarker();
					}
				}
			}
    }

	void InstantiateMarker()
    {
        //instantiate the marker prefab game object  marker = game object prefab, hit.point = where you click the mouse, transform rotation = transform rotation of this game object
        markerPrefab = (GameObject) Instantiate(marker, hit.point, transform.rotation);
    }

	void RemoveMarker()
    {
        Destroy(markerPrefab);
    }

	void PickUpItem()
	{
		//Destroy(target);
		//inventory.AddItem(target.GetComponent<ObjectID>().itemID);
		
		//  if (target != null)
	   	// {
		// 	float distance = Vector3.Distance (transform.position, target.transform.position);

		// 	if (distance <= 1.0f)
		// 	{
		// 		Destroy(target);
		// 		target = null;
		// 	}

		// }
	}

		

}
