﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FSM : MonoBehaviour
{
        //Player Transform
        protected Transform playerTransform;
        //Next destination position of the Box
        protected Vector3 destPos;
       
      
        //List of points for patrolling
        public float Health = 20;
        public float Level = 1;
        
        public float Exp = 0;
        public float MaxHealth = 20;
        public float Damage = 5;
        public float Vitality = 5;
        public float Strength = 5;
        public float SkillDamage = 20;
       
        protected virtual void Initialize (){
        }
        protected virtual void FSMUpdate (){
        }
        protected virtual void FSMFixedUpdate (){
        }
        void Start ()
        {
                Initialize ();
        }
        void Update ()
        {
                FSMUpdate ();
        }
        void FixedUpdate ()
        {
                FSMFixedUpdate ();
        }
}