﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

	
	//amount of coin
	public int amount;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player")
		{
			//dadagdag yung gold amount kapag pumasok sa trigger ng gameobject
			col.GetComponent<KokoFSM>().GoldAmount += amount;
			Destroy(gameObject);
		}
	}
}
