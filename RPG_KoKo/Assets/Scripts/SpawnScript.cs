﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour {

	 public GameObject Koko;
	 public GameObject[] prefab;
     public GameObject[] gos;

	 public int playerCount = 0;

	 

	 public GameObject[] spawnPoints;
	 float timer = 60;

     void Start()
     {	
		 //DITO MAGSSPAWN YUNG MGA ENEMIES
		 spawnPoints = GameObject.FindGameObjectsWithTag("WandarPoint");

		//ibig sabihin nito yung first instantiate ng objects magiging 30
		 gos = new GameObject[30];
		 //GOS.LENGTH IS EQUAL TO 30 adjust mo na lang yung number of instantiation kung gusto mo mas konti or marami 
         for(int i = 0; i < gos.Length; i++)
         {   
			 //instantiate prefab[randomb.range] ibig sabihin nyan kada loop nitong loop na to iba iba yung int na depende sa length ng prefab
			 //for example kapag 0 yung lumabas sa first loop yung goblin lalabas since siya yung nasa first list ng array which is eqaul to 0
				GameObject clone = (GameObject) Instantiate(prefab[Random.Range(0,prefab.Length)], spawnPoints[Random.Range(0,spawnPoints.Length)].transform.position, Quaternion.identity);
				gos[i] = clone;
         }
     }

	 void Update()
	 {
		 //ETO NAMAN YUNG TIMER GETS MO NA TO ALAM KO HAHA YUNG TIMER IS EQUAL TO 10 TAPOS KADA UPDATE MABABAWASANA SIYA NG 1 PER SECOND
		 timer -= Time.deltaTime;

			//KAPAG NAG 0 NA YUNG TIMER MAG IINSTANTIATE ULIT NG 10 ENEMY BASE DUN SA LOOP
		 if (timer <= 0)
		 {
			 for(int j = 0; j < 10; j++)
			 {
				GameObject clone = (GameObject) Instantiate(prefab[Random.Range(0,prefab.Length)], spawnPoints[Random.Range(0,spawnPoints.Length)].transform.position, Quaternion.identity);
				gos[j] = clone; 
			 }
			 //magiging 10 yung timer adjust mo na lang to or gawan mo ng variable para nammodify mo sa inspector 
			 timer = 10;
		 }
	 }

	 public void RespawnPlayer()
	 {
		 Debug.Log("running01");
		 if (playerCount < 1)
		 {
			 Debug.Log("running");
			  GameObject p = (GameObject) Instantiate(Koko, transform.position, Quaternion.identity);
			  playerCount++;
		 }
		 
	 }
}
