﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDataBase : MonoBehaviour {

	public List<Item> items = new List<Item>();

	void Awake()
	{
        //item database information
		items.Add(new Item("Potion1",        		0, 	"Restore 50 Health" ,   50, 30, Item.ItemType.Consumable));
		items.Add(new Item("Potion2",           	1, 	"Restore 50 Mana" ,     50, 30, Item.ItemType.Consumable));
        items.Add(new Item("Potion3",				2, 	"Restore 100 Health", 	100, 60, Item.ItemType.Consumable));
		items.Add(new Item("Potion4",				3,	"Restore 100 Mana" , 	 100, 60, Item.ItemType.Consumable));
        items.Add(new Item("Attack Potion",         4,  "Buff 50 Attack" ,      50, 40, Item.ItemType.Consumable));
        items.Add(new Item("Defense Potion",        5,  "Buff 50 Defense" ,     50, 40, Item.ItemType.Consumable));
        items.Add(new Item("Greater Attack Potion", 6,  "Buff 100 Attack" ,    100, 70, Item.ItemType.Consumable));
        items.Add(new Item("Greater Defense Potion",7,  "Buff 100 Defense" ,   100, 70, Item.ItemType.Consumable));
	}
}