﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{

     
    public List<Item> inventory = new List<Item>();
    public List<Item> slots = new List<Item>();

    
    private int xSlots = 5, ySlots = 5;

    
    private bool showInventory;

    
    private bool showToolTip;

    
    private string toolTip;

    
    public ItemDataBase database;

    
    public bool draggingItem;
    public Item draggedItem;
    public int prevIndex;

    void Start()
    {

        
        for (int i = 0; i < (xSlots * ySlots); i++)
        {
            
            slots.Add(new Item());
            inventory.Add(new Item());
        }


        database = GameObject.Find("ItemDataBase").GetComponent<ItemDataBase>();

        print(InventoryContains(0));

        
        AddItem(0);
    }


    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.I))
        {
            Debug.Log("I PRESSED");
            showInventory = !showInventory;
        }
    }

    
    string CreateToolTip(Item item)
    {
        toolTip = string.Format("<color=white>{0}</color>, \n \n , <color=red>{2}</color>", item.itemName, "", item.itemDescr);
        return toolTip;
    }

    
    public void AddItem(int id)
    {

        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].itemName == null)
            {
                for (int j = 0; j < database.items.Count; j++)
                {
                    if (database.items[j].itemID == id)
                    {
                        inventory[i] = database.items[j];
                    }
                }

                break;
            }
        }
    }

    //removing ng item
    void RemoveItem(int id)
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].itemID == id)
            {
                inventory[i] = new Item();
                break;
            }
        }
    }

    
    bool InventoryContains(int id)
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].itemID == id) return true;
        }
        return false;
    }

   


    private void UseConsumable(Item item, int slot, bool deleteItem)
    {
        KokoFSM KokoFSM = GameObject.FindObjectOfType<KokoFSM>();

        switch (item.itemID)
        {
            case 0:
                {
                    print("USED  CONSUMABLE " + item.itemName + " " + item.itemEffect);
                    KokoFSM.Health += item.itemEffect;
                    break;
                }

            case 1:
                {
                    print("USED  CONSUMABLE " + item.itemName + " " + item.itemEffect);
                    KokoFSM.Mana += item.itemEffect;
                    break;
                }

            case 2:
                {
                    print("USED  CONSUMABLE " + item.itemName + " " + item.itemEffect);
                    KokoFSM.Health += item.itemEffect;
                    break;
                }

            case 3:
                {
                    print("USED  CONSUMABLE " + item.itemName + " " + item.itemEffect);
                    KokoFSM.Mana += item.itemEffect;
                    break;
                }

        }

        if (deleteItem)
        {
            inventory[slot] = new Item();
        }

    }

}