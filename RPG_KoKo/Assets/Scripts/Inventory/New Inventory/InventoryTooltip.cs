﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryTooltip : MonoBehaviour
{
    [SerializeField] Text text;
    [SerializeField] Vector3 offset;

    public Text Text { get { return text; } }

    void Update()
    {
        transform.position = Input.mousePosition + offset;
    }
}
