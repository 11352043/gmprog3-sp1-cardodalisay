﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Data for tracking slots and their item in inventory UI.
/// </summary>
public class Slot
{
    public Button button;
    public Image slotIcon;
    public Item item;

    public Slot(Button button, Image slotIcon, Item item)
    {
        this.button = button;
        this.slotIcon = slotIcon;
        this.item = item;

        // When slot has already icon, set icon sprite
        this.slotIcon.sprite = item == null ? null : item.itemIco;
    }

    public void Reset()
    {
        slotIcon.sprite = null;
        item = null;
    }
}

public class NewInventory : MonoBehaviour
{
    [SerializeField] GameObject panel;              // Holds the whole inventory panel.
    [SerializeField] InventoryTooltip tooltip;
    [SerializeField] Image dragIcon;
    [SerializeField] List<GameObject> pages;        // Must be in order in inspector
    [SerializeField] Text pageNumber;
    [SerializeField] Button nextPageButton;
    [SerializeField] Button prevPageButton;
    [SerializeField] ItemDataBase itemDataBase;

    List<Button> buttons = new List<Button>();
    List<Slot> inventorySlots = new List<Slot>();

    int pageIndex = 0;
    Slot hoveredSlot;

    void Start()
    {
        // Get all buttons from pages and add to buttons list
        foreach (var page in pages)
            foreach (Transform child in page.transform)
                buttons.Add(child.GetComponent<Button>());

        // Register each buttons to inventorySlots
        foreach (var button in buttons)
        {
            var slotIcon = button.transform.Find("Image").GetComponent<Image>();
            // Initialize slot
            var slot = new Slot(button, slotIcon, null);
            // Register onClick event from button
            slot.button.onClick.AddListener(delegate { OnItemButtonClick(slot); });
            // Hover feature
            var eventTrigger = slot.button.GetComponent<EventTrigger>();
            {
                var entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerEnter;

                entry.callback.AddListener(delegate { OnSlotPointerEnter(slot); });
                eventTrigger.triggers.Add(entry);
            }
            {
                var entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerExit;

                entry.callback.AddListener(delegate { OnSlotPointerExit(slot); });
                eventTrigger.triggers.Add(entry);
            }
            // Drag feature
            {
                var entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.Drag;

                entry.callback.AddListener(delegate { OnSlotDrag(slot); });
                eventTrigger.triggers.Add(entry);
            }
            {
                var entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.EndDrag;

                entry.callback.AddListener(delegate { OnSlotEndDrag(slot); });
                eventTrigger.triggers.Add(entry);
            }

            // Add to inventorySlots
            inventorySlots.Add(slot);
        }

        // Show first page
        pages.ForEach(p => p.SetActive(false));
        pages[pageIndex].SetActive(true);
        // Disable tooltip and drag
        tooltip.gameObject.SetActive(false);
        dragIcon.gameObject.SetActive(false);
        // Assign buttons for next page and prev page
        nextPageButton.onClick.AddListener(NextPage);
        prevPageButton.onClick.AddListener(PrevPage);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            panel.SetActive(!panel.activeSelf);
        }
    }

    public void AddItem(int id)
    {
        // Get first slot that has no item.
        var slot = inventorySlots.First(i => i.item == null);
        var item = itemDataBase.items.Find(i => i.itemID == id);
        // If no slot is available, exit method.
        if (slot == null)
        {
            Debug.Log("Inventory full.");
            return;
        }
        if (item == null)
        {
            Debug.Log("Item ID: " + id + " not found.");
            return;
        }

        slot.item = item;
        slot.slotIcon.sprite = item.itemIco;
    }

    public void RemoveItem(Item item)
    {
        var slot = inventorySlots.Find(i => i.item.itemID == item.itemID);

        if (slot == null)
        {
            Debug.Log("No itemID " + item.itemID + " found.");
            return;
        }

        slot.item = null;
        slot.slotIcon = null;
    }

    /// <summary>
    /// Called when OnClicked event from button is triggered.
    /// </summary>
    /// <param name="slot"> Inventory slot button that is triggered.</param>
    public void OnItemButtonClick(Slot slot)
    {
        var item = slot.item;

        if (item == null)
            return;

        //kapag consumable yung potion
        if (item.itemType == Item.ItemType.Consumable)
        {
            UseConsumable(item);
            slot.Reset();
        }
    }

    public void OnSlotPointerEnter(Slot slot)
    {
        hoveredSlot = slot;
        if (slot.item == null)
            return;

        tooltip.Text.text = slot.item.itemDescr;
        tooltip.gameObject.SetActive(true);

    }

    public void OnSlotPointerExit(Slot slot)
    {
        hoveredSlot = null;
        tooltip.Text.text = "";
        tooltip.gameObject.SetActive(false);
    }

    public void OnSlotDrag(Slot slot)
    {
        if (slot.item == null)
            return;
        // Hide tooltip
        tooltip.gameObject.SetActive(false);
        // Show Icon of object
        dragIcon.gameObject.SetActive(true);
        dragIcon.sprite = slot.slotIcon.sprite;
        // Follow Mouse
        dragIcon.transform.position = Input.mousePosition;
    }

    public void OnSlotEndDrag(Slot slot)
    {
        // Hide item
        dragIcon.gameObject.SetActive(false);
        // If mouse raycast has slot:
        if (hoveredSlot != null)
        {
            // If same slot
            if (slot == hoveredSlot)
                return;
            // If slot empty
            if (hoveredSlot.item == null)
            {
                hoveredSlot.item = slot.item;
                hoveredSlot.slotIcon.sprite = slot.slotIcon.sprite;
                slot.Reset();
            }
            // If slot filled, swap
            else
            {
                var hoveredSlotItem = hoveredSlot.item;
                hoveredSlot.item = slot.item;
                hoveredSlot.slotIcon.sprite = slot.slotIcon.sprite;
                slot.item = hoveredSlotItem;
                slot.slotIcon.sprite = slot.item.itemIco;
            }

        }

    }

    void UseConsumable(Item item)
    {
        KokoFSM KokoFSM = GameObject.FindObjectOfType<KokoFSM>();

        switch (item.itemID)
        {
            case 0:
                {
                    print("USED  CONSUMABLE " + item.itemName + " " + item.itemEffect);
                    KokoFSM.Health += item.itemEffect;
                    break;
                }

            case 1:
                {
                    print("USED  CONSUMABLE " + item.itemName + " " + item.itemEffect);
                    KokoFSM.Mana += item.itemEffect;
                    break;
                }

            case 2:
                {
                    print("USED  CONSUMABLE " + item.itemName + " " + item.itemEffect);
                    KokoFSM.Health += item.itemEffect;
                    break;
                }

            case 3:
                {
                    print("USED  CONSUMABLE " + item.itemName + " " + item.itemEffect);
                    KokoFSM.Mana += item.itemEffect;
                    break;
                }
        }
    }

    public void NextPage()
    {
        pageIndex = pageIndex >= pages.Count - 1 ? 0 : pageIndex + 1;
        UpdatePage();
    }

    public void PrevPage()
    {
        pageIndex = pageIndex <= 0 ? pages.Count - 1 : pageIndex - 1;
        UpdatePage();
    }

    void UpdatePage()
    {
        // Disable all pages
        pages.ForEach(p => p.SetActive(false));
        // Enable current page
        pages[pageIndex].SetActive(true);
        // Update page text
        pageNumber.text = (pageIndex + 1).ToString();
    }
}