﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class Item  {

	public string itemName;
	public int itemID;
	public string itemDescr;
	public Sprite itemIco;
	public int itemEffect;
    public int itemPrice;
	public ItemType itemType;

	public enum ItemType
	{
		Weapon,
		Consumable,
		Quest
	}
	
	public Item(string name, int id, string descr, int effect, int price, ItemType type)
	{
		itemName = name;
		itemID = id;
		itemDescr = descr;
		itemIco = Resources.Load<Sprite> ("Item Icons/" + name);
		itemEffect = effect;
        itemPrice = price;
		itemType = type;
	}

	public Item()
	{

	}
}