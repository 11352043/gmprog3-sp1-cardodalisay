﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorManager : MonoBehaviour {

	public GameObject[] CharacterList;
	public int currID;
	public Animator animator;
	public GameObject[] animationButton;
	GameObject currButton;

	void Start()
	{
		animator =	GetComponent<Animator>();
	}

	public virtual void CharacterSelect(int id)
	{
		// iccancel yung function na monster selector kase dahil tatlong monster yung lumalabas kaya need gumawa ng bagong function para dun 
		MonsterSelector(false);

		//yung current id for example 0 kung titingnan mo yung array ng characterlist nasa animator controller, nasa 0 si koko so kapag 0 yung currID si koko yung lalabas
		//setactive false para madisable yung current character na naka show  
		CharacterList[currID].SetActive(false);

		//currid = id kung titingnan mo yung parameter na id dyan pinapasa yung paramater sa buttons ng playerbutton, npcbutton, tsaka monsterbutton tingnan mo yung animationcanvas para makita mo
		currID = id;

		//kapag pindot mo ng button for example yung playerbutton yung parameter niya sa button is 0 kapag pinindot mo yun yung magiging active na characterlist is si koko kapag yung 
		//monsteranim naman pinindot mo which is parameter nun is 2 lalabas yung tatlong monster dahil sa monster selector function
		CharacterList[currID].SetActive(true);

		//kapag hindi null yung current button ibig sabihin kapag hindi siya empty gagawing false yung current button para dun sa bagong button na papalitan
		if (currButton != null)
		{
			//gagawing false yung currentbutton ibig sabihin mawawala muna yung animation button sa scene
			currButton.SetActive(false);	
		}

		//animationbutton[currid] kung ano yung pinasa sa function na characterselect yun din yung magiging parameter ng animationbutton
		//forexample 0 yung paramater na pinasa dito sa characterselect ang mangyayari yung animation button ni koko yung lalabas since yung nasa pinakaumpisa ng array ng animationbutton
		//is yung player button
		animationButton[currID].SetActive(true); 
		currButton = animationButton[currID].gameObject;

		//monster selector 2 is para madetect naman kung yung currid is equal to 2, kapag equal to 2 siya lalabas yung tatlong monster dahil dun sa list sa function na monsterselector
		MonsterSelector(true);
	}

	//eto yung function na monster selector
	public void MonsterSelector(bool show)
	{
		//kapag yung currid niya is equal to 2
		if (currID == 2)
		{
			//eto yung for loop which is kukunin yung lahat ng character na maguumpisa sa element 2 kaya 2 yung nandyan
			//yung nasa element 2 is yung goblin so kapag nag run yung loop na to lahat ng 2 pataas ippaakita sa scene dahil sa loop na to
			//characterlist[i] is ibig sabihin lahat ng 2 pataas magiging active
			for (int i = 2; i < CharacterList.Length; i++)
			{
				//eto yung pag active nung character katulad nung nakaindicate dun sa taas
				CharacterList[i].SetActive(show);	
			}

			//kapag currid is equal to 2 yung lalabas na animationbutton is yung nasa element 2 ng animation button which is yung monster button
			animationButton[2].SetActive(true); 
		}
	}

	//etong mga paramaters na to ginagamit kapag nagsend ka ng information sa animator
	//for example animator.setbool("isidle, true) magiging true yung nasa animator ng pipindutin mo na button 
	//yung sa blend naman papalitan yung value ng blend para maging smooth yung transition 
	//depende sa animation for example magkasama yung run and walk 
	//kapag 0 ibig sabihin walk yun tapos kapag 1 magiging run
	//check mo yung animator controller tapos pindutin mo yung run or yung attack para makita mo anong nangyayari sa blend value
	
	//naka virtual void lahat ng mga to para maging accsesisble yung function na to sa ibang class tapos pwede mo imodify
	//for example etong run function, kapag nag change anim ka syempre kailangan mo na ifalse yung parameter nito 
	//ang gagawin mo kapag nasa new class ka na gagawa ka ng function para macancel yung idle 
	//yung function na yun irrun mo sa loob ng new function tingnan mo yung monster script nandun yung explanation para dito

	//yung sa state ganun din for example cnlick mo yung run button ng monster makikita mo sa button may paramater dun 
	//ipapasa yung value ng paramater dun dito sa function para mamodify yung nasa animator 


	public virtual void Talk()
	{
		animator.SetBool("IsTalking", true);
	}

	public virtual void Idle()
	{
		animator.SetBool("IsIdle", true);
	}

	public virtual void Run(float value)
	{
		animator.SetFloat("Blend", value);
		animator.SetBool("IsRunning", true);	
	}

	public virtual void Attack(float value)
	{
		animator.SetFloat("Blend", value);
		animator.SetBool("IsAttacking", true);
	}

	public virtual void Damaged(bool state)
	{
		animator.SetBool("IsDamaged", state);
	}
	
	public virtual void Dead(bool state)
	{
		animator.SetBool("IsDead", state);
	}

	public virtual void DrawBlade(float value)
	{
		animator.SetFloat("Blend", value);
		animator.SetBool("IsDrawBlade", true);
	}

	public virtual void PutBlade(float value)
	{
		animator.SetFloat("Blend", value);
		animator.SetBool("IsPutBlade", true);
	}

	public virtual void CastSkill(bool state)
	{
		animator.SetBool("IsCastingSkill", true);
	}
}
