﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rewards : MonoBehaviour {

	public GameObject[] Drops;
	public GameObject Coin;
	public GameObject ItemDropped;
	public int[] dropRate;

	public void DropLoot() {
		float roll;
		roll = Random.Range (0f, 101f);
		if (roll > dropRate[0]) {
			ItemDropped = Drops [0];
		}

		else if (roll > dropRate[1]) {
			ItemDropped = Drops [1];
		}

		else if (roll > dropRate[2]) {
			ItemDropped = Drops [2];
		}

		else {
			ItemDropped = null;
			Debug.Log ("NO DROPPPP!!");
		}
	}
}
