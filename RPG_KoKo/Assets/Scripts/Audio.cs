﻿using UnityEngine;
using System.Collections;

public class Audio : MonoBehaviour
{
    public AudioClip SwordSwing;
    private AudioSource source;
    private float volLowRange = .5f;
    private float volHighRange = 1.0f;


    void Awake()
    {

        source = GetComponent<AudioSource>();

    }
    void Update()
    {

        if (Input.GetButtonDown("Fire1"))
        {
            float vol = Random.Range(volLowRange, volHighRange);
            source.PlayOneShot(SwordSwing, vol);
        }

    }
}