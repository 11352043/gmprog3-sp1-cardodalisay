﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObjectID : MonoBehaviour {

	public NewInventory Inventory;
	public int ID;

	void Start()
	{
	  Inventory = GameObject.FindObjectOfType<NewInventory>();
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player")
		{
			AddItem(ID);
			Destroy(gameObject	);
		}
	}

	public void AddItem(int id)
	{	
		Inventory.AddItem(id);
	}
}

