﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KokoAnim : MonoBehaviour
{

    public Animator animator;

    public bool IsIdle;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void AnimState(int anim, float blend)
    {
       AnimationTransition(anim);
       BlendTransition(blend);
    }

    public void AnimationTransition(int value)
    {
        animator.SetInteger("AnimationID", value);
    }

    public void BlendTransition(float value)
    {
        animator.SetFloat("Blend", value);
    }

}
